package com.vityalimbaev.luxside;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.transition.ChangeBounds;
import android.transition.Slide;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.constraintlayout.widget.Guideline;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class AnimationFactory {
    public static final int START_TOOLBAR_ANIMATION = 0;
    public static final int REVERSE_TOOLBAR_ANIMATION = 1;
    public static final int ALPHA1 = 255;
    public static final int ALPHA2 = 30;

    public static boolean cardViewSizeState = false;
    public static int animStartColor;
    public static int animEndColor;

    public static void initToolBarAnim(ViewGroup viewGroup, int duration, int direction) {

        int start = (direction == START_TOOLBAR_ANIMATION) ? ALPHA1 : ALPHA2;
        int end = (direction == START_TOOLBAR_ANIMATION) ? ALPHA2 : ALPHA1;

        final ValueAnimator colorAnim = ObjectAnimator.ofInt(start, end);
        colorAnim.addUpdateListener(animation -> {
            int mul = (int) animation.getAnimatedValue();
            viewGroup.getBackground().setAlpha(mul);
        });

        colorAnim.setInterpolator((direction == START_TOOLBAR_ANIMATION) ?
                new DecelerateInterpolator(2.0f) : new AccelerateInterpolator());
        colorAnim.setDuration(duration);
        colorAnim.setRepeatMode(ObjectAnimator.RESTART);
        colorAnim.setRepeatCount(0);
        colorAnim.start();
    }

    public static void initSwitchColorLampAnim(View view) {
        Drawable background = view.getBackground();
        if (background instanceof ColorDrawable)
            animStartColor = ((ColorDrawable) background).getColor();

        ObjectAnimator objectAnimator = ObjectAnimator.ofInt(
                view,
                "backgroundColor",
                animStartColor,
                animEndColor
        ).setDuration(500);
        objectAnimator.setEvaluator(new ArgbEvaluator());
        objectAnimator.start();
    }

    public static void initScalePreviewAnim(View view, CardView cardView, ConstraintLayout constraintLayout,
                                            Guideline upGuideline, Guideline downGuideline) {

        Guideline[] lines = new Guideline[2];
        AppUtility.<Guideline>getViews(view, Guideline.class, R.id.upGuidline, R.id.downGuidline).toArray(lines);
        float size = lines[1].getY() - lines[0].getY();

        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone((ConstraintLayout) cardView.getParent());

        constraintSet.clear(cardView.getId(), ConstraintSet.TOP);
        constraintSet.clear(cardView.getId(), ConstraintSet.BOTTOM);

        if (!cardViewSizeState) {
            constraintSet.connect(cardView.getId(), ConstraintSet.BOTTOM, downGuideline.getId(), ConstraintSet.TOP, 0);
            constraintSet.connect(cardView.getId(), ConstraintSet.TOP, upGuideline.getId(), ConstraintSet.BOTTOM, 0);
            constraintSet.setDimensionRatio(cardView.getId(), "1");
            cardView.setRadius(size / 2.0f);
            Log.d("CARD", cardView.getWidth() + " " + cardView.getHeight());

        } else {
            constraintSet.connect(cardView.getId(), ConstraintSet.BOTTOM, constraintLayout.getId(), ConstraintSet.BOTTOM, 0);
            constraintSet.connect(cardView.getId(), ConstraintSet.TOP, constraintLayout.getId(), ConstraintSet.TOP, 0);
            constraintSet.setDimensionRatio(cardView.getId(), "3:4");
            cardView.setRadius(20 * Resources.getSystem().getDisplayMetrics().density);
        }

        constraintLayout.setConstraintSet(constraintSet);
        cardViewSizeState = !cardViewSizeState;

        ChangeBounds ct = new ChangeBounds();
        ct.setDuration(100);
        ct.setInterpolator(new LinearInterpolator());

        TransitionManager.beginDelayedTransition(constraintLayout, ct);
        constraintSet.applyTo(constraintLayout);

    }

    public static void initSettingsBarAnim(ConstraintLayout settingsBar, ConstraintLayout cardViewContainer, boolean visible) {
        Slide slide = new Slide();
        slide.setSlideEdge(Gravity.BOTTOM);
        slide.setDuration(300);
        slide.setInterpolator(new LinearInterpolator());

        ChangeBounds ct = new ChangeBounds();
        ct.setDuration(100);
        ct.setInterpolator(new LinearInterpolator());

        TransitionManager.beginDelayedTransition(cardViewContainer, ct);
        TransitionManager.beginDelayedTransition((ViewGroup) settingsBar.getParent(), ct);
        TransitionManager.beginDelayedTransition((ViewGroup) settingsBar, slide);
        if (visible)
            settingsBar.setVisibility(View.GONE);
        else
            settingsBar.setVisibility(View.VISIBLE);

    }

    public static void createShopCircleAnimation(View container) {
        int duration = 15;
        ArrayList<ImageView> arrPlanet = AppUtility.<ImageView>getViews(container, ImageView.class,
                R.id.whiteLampBtn,
                R.id.warmLampBtn,
                R.id.coldLampBtn,
                R.id.pinkLampBtn,
                R.id.purpleLampBtn,
                R.id.blueLampBtn,
                R.id.greenLampBtn,
                R.id.redLampBtn,
                R.id.turquoiseLampBtn);

        for (ImageView planet : arrPlanet) {
            animatePlanet(planet, TimeUnit.SECONDS.toMillis(duration)).start();
        }

    }

    private static ValueAnimator animatePlanet(ImageView planet, long orbitDuration) {
        ConstraintLayout.LayoutParams offsetLayoutParams = (ConstraintLayout.LayoutParams) planet.getLayoutParams();
        float offset = offsetLayoutParams.circleAngle;
        ValueAnimator anim = ValueAnimator.ofFloat(offset, 359.0f + offset);

        anim.setDuration(orbitDuration);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatMode(ValueAnimator.RESTART);
        anim.setRepeatCount(ValueAnimator.INFINITE);
        anim.addUpdateListener(valueAnimator -> {
            ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) planet.getLayoutParams();
            layoutParams.circleAngle = (float) valueAnimator.getAnimatedValue();
            planet.setLayoutParams(layoutParams);
        });

        return anim;
    }
}
