package com.vityalimbaev.luxside;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.view.View;

import androidx.core.content.ContextCompat;

import java.io.File;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class AppUtility {
    private static final String APP_PREFERENCES = "galopreferences";
    public static final String NAME = "testex";
    public static final String SHOW_ALERT_DIALOG = "show_alert_Dialog";
    public static final String RESULT_SHOP_KEY = "flag";
    public static final int REQUEST_SHOP = 100;
    public static final int RESULT_SHOP = 200;
    private static final int MAX_DISPLAY_BRIGHTNESS = 255;

    private static int brightness;

    protected static <T> ArrayList<T> getViews(View parent, Class<T> typeView, int... ids) {
        ArrayList<T> views = new ArrayList<>();
        for (int id : ids) {
            views.add(typeView.cast(parent.findViewById(id)));
        }
        return views;
    }

    protected static void saveUserSettings(String key, Boolean value, Context context) {
        SharedPreferences sPref = context.getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putBoolean(key, value);
        ed.apply();
    }

    protected static Boolean loadUSerSettings(String key, Context context) {
        SharedPreferences sPref = context.getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
        return sPref.getBoolean(key, false);
    }

    protected static void hideSystemUI(Activity activity) {
        final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

        activity.getWindow().getDecorView().setSystemUiVisibility(flags);

        final View decorView = activity.getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener(visibility -> {
            if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                decorView.setSystemUiVisibility(flags);
            }
        });

    }

    public static int getColor(int idColor, Context context) {
        return ContextCompat.getColor(context, idColor);
    }

    public static Drawable getDrawable(int idDrawable, Context context) {
        return ContextCompat.getDrawable(context, idDrawable);
    }

    public static File saveImageFile() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ROOT)
                .format(new Date());
        String fileName = "IMG_" + timeStamp + ".jpg";

        String path = Environment.DIRECTORY_DCIM.concat("/Camera");
        return new File(Environment.getExternalStoragePublicDirectory(path),
                fileName);
    }

    public static int getLampColor(int id, Context context) {
        switch (id) {
            case R.id.warmLampBtn:
                return ContextCompat.getColor(context, R.color.colorWarmLamp);
            case R.id.coldLampBtn:
                return ContextCompat.getColor(context, R.color.colorColdLamp);
            case R.id.pinkLampBtn:
                return ContextCompat.getColor(context, R.color.colorPinkLamp);
            case R.id.purpleLampBtn:
                return ContextCompat.getColor(context, R.color.colorPurpleLamp);
            case R.id.blueLampBtn:
                return ContextCompat.getColor(context, R.color.colorBlueLamp);
            case R.id.redLampBtn:
                return ContextCompat.getColor(context, R.color.colorRedLamp);
            case R.id.greenLampBtn:
                return ContextCompat.getColor(context, R.color.colorGreenLamp);
            case R.id.turquoiseLampBtn:
                return ContextCompat.getColor(context, R.color.colorTurquoiseLamp);
            default:
                return ContextCompat.getColor(context, R.color.colorWhite);
        }
    }

    protected static void displayBrightness(boolean stateLamp, Context context) {
        if (stateLamp) {
            try {
                Settings.System.putInt(context.getContentResolver(),
                        Settings.System.SCREEN_BRIGHTNESS_MODE,
                        Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);

                brightness = Settings.System.getInt(context.getContentResolver(),
                        Settings.System.SCREEN_BRIGHTNESS);
                setBrightness(MAX_DISPLAY_BRIGHTNESS, context);
            } catch (Settings.SettingNotFoundException e) {
                Log.e("Error", "Cannot access system brightness");
                e.printStackTrace();
            }
        } else {
            setBrightness((brightness <= 0 || brightness > 255) ? MAX_DISPLAY_BRIGHTNESS : brightness, context);
        }
    }

    protected static void setBrightness(int brightness, Context context) {
        if (brightness < 0)
            brightness = 0;
        else if (brightness > 255)
            brightness = 255;

        ContentResolver cResolver = context.getApplicationContext().getContentResolver();
        Settings.System.putInt(cResolver, Settings.System.SCREEN_BRIGHTNESS, brightness);
    }

    public static int adjustAlpha(int color, int alpha) {
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        return Color.argb(alpha, red, green, blue);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = Math.min(heightRatio, widthRatio);
        }

        return inSampleSize;
    }

    public static byte[] getBitmapFromJPEG(Image image) {
        ByteBuffer byteBuffer = image.getPlanes()[0].getBuffer();
        byteBuffer.rewind();
        byte[] byteArray = new byte[byteBuffer.capacity()];
        byteBuffer.get(byteArray);
        return byteArray;
    }

    public static Bitmap rotateBitmap(Bitmap bitmap, int reqRotation, int curRotation) {
        Matrix rotationMatrix = new Matrix();
        int rotateAngle;
        switch (curRotation) {
            case 270:
                rotateAngle = -90;
                break;
            case 180:
                rotateAngle = -180;
                break;
            case 90:
                rotateAngle = -270;
                break;
            default:
                rotateAngle = 0;
        }
        rotationMatrix.postRotate(rotateAngle);

        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(),
                rotationMatrix, true);
    }
}
