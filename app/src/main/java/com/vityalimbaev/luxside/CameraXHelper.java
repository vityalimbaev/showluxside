package com.vityalimbaev.luxside;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Matrix;
import android.view.Surface;
import android.view.TextureView;
import android.widget.ImageButton;

import androidx.camera.core.AspectRatio;
import androidx.camera.core.CameraX;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureConfig;
import androidx.camera.core.Preview;
import androidx.camera.core.PreviewConfig;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.LifecycleOwner;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

class CameraXHelper {

    private Context context;
    private TextureView textureView;
    private ImageButton cameraImageButton;
    private ImageCapture imageCapture;
    private boolean state = false;

    CameraXHelper(Context context, TextureView textureView, ImageButton cameraImageButton) {
        this.context = context;
        this.textureView = textureView;
        this.cameraImageButton = cameraImageButton;
    }

    void startCamera() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        updateTransform();
        Preview preview = buildPreview();
        imageCapture = buildImageCapture();

        CameraX.bindToLifecycle((LifecycleOwner) context,
                preview,
                imageCapture);
    }

    public Executor executor = Executors.newSingleThreadExecutor();

    private Preview buildPreview() {
        PreviewConfig.Builder previewConfig = new PreviewConfig.Builder();
        previewConfig.setTargetAspectRatio(AspectRatio.RATIO_4_3);
        previewConfig.setLensFacing(CameraX.LensFacing.FRONT);
        Preview preview = new Preview(previewConfig.build());
        preview.setOnPreviewOutputUpdateListener(output -> {
            textureView.setSurfaceTexture(output.getSurfaceTexture());
        });
        return preview;
    }

    private ImageCapture buildImageCapture() {
        ImageCaptureConfig imageCaptureConfig = new ImageCaptureConfig.Builder()
                .setTargetAspectRatio(AspectRatio.RATIO_4_3)
                .setCaptureMode(ImageCapture.CaptureMode.MAX_QUALITY)
                .setLensFacing(CameraX.LensFacing.FRONT)
                .setTargetRotation(Surface.ROTATION_0)
                .build();
        return new ImageCapture(imageCaptureConfig);
    }

    public ImageCapture getImageCapture(){
        return imageCapture;
    }

    void updateTransform() {
        Matrix matrix = new Matrix();

        float w = textureView.getWidth();
        float h = textureView.getHeight();
        float centerX = w / 2f;
        float centerY = h / 2f;

        if (state) {
            matrix.preScale(1, 4.0f / 3.0f, centerX, centerY);
        } else {
            matrix.preScale(1, 1, centerX, centerY);
        }

        textureView.setTransform(matrix);
        state = !state;
    }

}
