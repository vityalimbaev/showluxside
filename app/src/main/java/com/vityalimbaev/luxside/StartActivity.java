package com.vityalimbaev.luxside;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageProxy;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class StartActivity extends AppCompatActivity {

    private final int ALPHA = 100;
    private final float LOW_ALPHA = 0.4f;
    private final float HEIGHT_ALPHA = 1.0f;

    private boolean stateLamp;
    private boolean showAlertDialog;
    private boolean stateApp;

    private ConstraintLayout settingsLayout;
    private ConstraintLayout toolbar;
    private ConstraintLayout mainLayout;
    private ImageButton takePictureButton;
    private ImageButton lampButton;
    private TextureView imageView;
    private CameraXHelper cameraHelper;
    private CardView cardView;

    protected ConstraintLayout cardViewContainer;
    protected Guideline upGuideLine;
    protected Guideline downGuideLine;

    private boolean resultOfSaveImg = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        AppUtility.hideSystemUI(this);
        AnimationFactory.animStartColor = this.getColor(R.color.colorAppBackground);
        AnimationFactory.animEndColor = this.getColor(R.color.colorWhite);

        toolbar = findViewById(R.id.toolbar);
        mainLayout = findViewById(R.id.container);
        settingsLayout = findViewById(R.id.settings);
        takePictureButton = findViewById(R.id.cuptureImgButton);
        lampButton = findViewById(R.id.lampBtn);
        imageView = findViewById(R.id.textureView);
        cameraHelper = new CameraXHelper(this, imageView, takePictureButton);
        cardView = findViewById(R.id.cardView);
        cardViewContainer = findViewById(R.id.containerCardView);
        upGuideLine = findViewById(R.id.upGuidline);
        downGuideLine = findViewById(R.id.downGuidline);

        stateLamp = false;
        showAlertDialog = AppUtility.loadUSerSettings(AppUtility.SHOW_ALERT_DIALOG, this);
        takePictureButton.setEnabled(false);

        stateApp = AppUtility.loadUSerSettings(AppUtility.NAME, this);
        switchStateApp();
        verifyPermissions();
        initTouchEventCardView();
    }

    public void captureImg(View view) {
        cameraHelper.getImageCapture().takePicture(cameraHelper.executor, new ImageCapture.OnImageCapturedListener() {
            @Override
            public void onCaptureSuccess(ImageProxy imageProxy, int rotationDegrees) {
                Log.d("MYROTATION", "onCaptureSuccess: "+rotationDegrees);
                byte[] bytes = AppUtility.getBitmapFromJPEG(imageProxy.getImage());
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);

                options.inSampleSize = AppUtility.calculateInSampleSize(options, imageView.getWidth(), imageView.getHeight());
                options.inJustDecodeBounds = false;

                DecodeByteArrayToBitmapAsyncTask decodeAsyncTask = new DecodeByteArrayToBitmapAsyncTask(options, Surface.ROTATION_0, rotationDegrees);
                try {
                    decodeAsyncTask.execute(bytes).get();
                } catch (ExecutionException | InterruptedException e) {
                    e.printStackTrace();
                }

            }
            @Override
            public void onError(@NonNull ImageCapture.ImageCaptureError imageCaptureError, @NonNull String message, @Nullable Throwable cause) {
                super.onError(imageCaptureError, message, cause);
            }
        });

    }

    public void showCaptured(Bitmap bitmap) {
        CardView card = findViewById(R.id.capimg);
        if (bitmap!= null) {
            Log.d("FFF","ок");
            ImageView myImage = (ImageView) findViewById(R.id.imageViewCapImg);
            myImage.setImageBitmap(bitmap);
            card.setVisibility(View.VISIBLE);
        }
    }

    public void settings(View v) {
        boolean visible = settingsLayout.getVisibility() == View.VISIBLE;
        AnimationFactory.initSettingsBarAnim(settingsLayout, cardViewContainer, visible);
    }


    public void openShop(View view) {
        Intent intent = new Intent(StartActivity.this, ShopActivity.class);
        startActivityForResult(intent, AppUtility.REQUEST_SHOP);
        overridePendingTransition(R.anim.alpha_activity_open, R.anim.alpha_activity_close);
    }

    public void switchColorLamp(View v) {
        if (v.getAlpha() == LOW_ALPHA) {
            openShop(v);
            return;
        }
        int colorRGB = AppUtility.getLampColor(v.getId(), this);
        int colorARGB = AppUtility.adjustAlpha(colorRGB, ALPHA);
        ViewCompat.setBackgroundTintList(
                lampButton,
                ColorStateList.valueOf(colorARGB));
        AnimationFactory.animEndColor = colorRGB;
        if (stateLamp) {
            switchStateLamps();
        }
    }

    public void enableLamp(View v) {
        if (!Settings.System.canWrite(getApplicationContext()) && !showAlertDialog && !stateLamp) {
            verifyPermissionOfBrightnessDisplay();
        }
        stateLamp = !stateLamp;
        AnimationFactory.initToolBarAnim(toolbar,
                500,
                (stateLamp) ? AnimationFactory.START_TOOLBAR_ANIMATION : AnimationFactory.REVERSE_TOOLBAR_ANIMATION);
        if (Settings.System.canWrite(getApplicationContext())) {
            AppUtility.displayBrightness(stateLamp, this);
        }

        switchStateLamps();
    }

    public void switchStateLamps() {
        if (stateLamp) {
            AnimationFactory.initSwitchColorLampAnim(mainLayout);

        } else {
            int saveColorEnd = AnimationFactory.animEndColor;
            AnimationFactory.animEndColor = ContextCompat.getColor(this, R.color.colorAppBackground);
            AnimationFactory.initSwitchColorLampAnim(mainLayout);
            AnimationFactory.animEndColor = saveColorEnd;
        }
    }

    private void verifyPermissionOfBrightnessDisplay() {
        View checkBoxView = View.inflate(this, R.layout.checkbox, null);
        CheckBox checkBox = (CheckBox) checkBoxView.findViewById(R.id.checkbox);
        checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            showAlertDialog = true;
        });
        checkBox.setText("Больше не показывать");


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Подсветка экрана");
        builder.setMessage("Приложение может автоматически увеличивать яркость экрана при включении подсветки");
        builder.setView(checkBoxView);

        builder.setPositiveButton("Продолжить", (dialog, which) -> {
            Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(intent);
            showAlertDialog = true;
        });
        builder.setNegativeButton("Отмена", null);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void verifyPermissions() {
        String[] permissions = {
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA};

        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                permissions[0]) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this.getApplicationContext(),
                permissions[1]) == PackageManager.PERMISSION_GRANTED) {

            takePictureButton.setEnabled(true);
            cameraHelper.startCamera();
        } else {
            ActivityCompat.requestPermissions(StartActivity.this,
                    permissions, 0);
        }
    }

    public void switchStateApp() {
        ArrayList<ImageButton> arrImageButtons = AppUtility.<ImageButton>getViews(toolbar, ImageButton.class,
                R.id.coldLampBtn,
                R.id.pinkLampBtn,
                R.id.purpleLampBtn,
                R.id.blueLampBtn,
                R.id.greenLampBtn,
                R.id.redLampBtn,
                R.id.turquoiseLampBtn);
        float alpha = (stateApp) ? HEIGHT_ALPHA : LOW_ALPHA;

        for (ImageButton imageButton : arrImageButtons) {
            imageButton.setAlpha(alpha);
        }
    }


    @SuppressLint("ClickableViewAccessibility")
    private void initTouchEventCardView() {
        cardViewContainer.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                new Handler(Looper.getMainLooper()).postDelayed(() -> cameraHelper.updateTransform(), 50);
                AnimationFactory.initScalePreviewAnim(findViewById(R.id.container), cardView, cardViewContainer, upGuideLine, downGuideLine);
                return true;
            }
            return false;
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        verifyPermissions();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null || requestCode != AppUtility.REQUEST_SHOP || resultCode != AppUtility.RESULT_SHOP) {
            return;
        }
        String response = data.getStringExtra(AppUtility.RESULT_SHOP_KEY);
        stateApp = Boolean.parseBoolean(response);
        AppUtility.saveUserSettings(AppUtility.NAME, stateApp, this);
        switchStateApp();
    }

    class DecodeByteArrayToBitmapAsyncTask extends AsyncTask<byte[],Integer,Bitmap> {
        BitmapFactory.Options options;
        int reqRotation;
        int curRotation;
        DecodeByteArrayToBitmapAsyncTask(BitmapFactory.Options options, int reqRotation, int curRotation){
            this.options = options;
            this.reqRotation = reqRotation;
            this.curRotation = curRotation;
        }

        @Override
        protected Bitmap doInBackground(byte[]... bytes) {
            return BitmapFactory.decodeByteArray(bytes[0],0, bytes[0].length, options);
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            showCaptured(AppUtility.rotateBitmap(result,reqRotation,curRotation));
            super.onPostExecute(result);
        }
    }

}
